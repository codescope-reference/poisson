package main

import "core:fmt"
import "core:math"
import "core:math/rand"

a :: proc(i: u64, j: u64, width: u64) -> f64 {
    if i == j {
        return -4
    }

    if i + 1 == j {
        return 1
    }

    if i - 1 == j {
        return 1
    }

    if i + width == j {
        return 1
    }

    if i - width == j {
        return 1
    }

    return 0
}

solve :: proc(field: ^Field2D, source: ^Field2D) {
    // triangle^2 output = source
    // Ax = b, A is a matrix, x is a vector, b is a vector

    // output = V(x, y)
    // output = V(x_i, y_i)
    // triangle^2 = d^2/dx^2 + d^2/dy^2
    // d/dx output = (V(x_i+1, y_i) - V(x_i, y_i)) / dx
    // d/dx output = (V(x_i, y_i) - V(x_i-1, y_i)) / dx
    // d/dx output = (V(x_i+1, y_i) - V(x_i-1, y_i)) / (2 * dx)
    // d^2/dx^2 output = ((V(x_i+1, y_i) - V(x_i, y_i)) / dx - ((V(x_i, y_i) - V(x_i-1, y_i)) / dx) / dx
    // d^2/dx^2 output = ((V(x_i+1, y_i) - V(x_i, y_i)) - ((V(x_i, y_i) - V(x_i-1, y_i))) / dx^2
    // d^2/dx^2 output = (V(x_i+1, y_i) - V(x_i, y_i) - V(x_i, y_i) + V(x_i-1, y_i)) / dx^2

    // d^2/dx^2 output = (V(x_i+1, y_i) + V(x_i-1, y_i) - 2 * V(x_i, y_i) ) / dx^2
    // d^2/dy^2 output = (V(x_i, y_i+1) + V(x_i, y_i-1) - 2 * V(x_i, y_i) ) / dy^2

    // V = [V(x_1, y_1), V(x_2, y_1), ..., V(x_width, y_1), V(x_1, y_2), V(x_2, y_2), ... ]
    // A_x = 1/ dx^2 [ [ -2, +1,  0,  ...],
    //               [ +1, -2, +1,  0, ...]
    //               [  0, +1, -2, +1, 0, ...]
    //               [  0,  0, +1, -2, +1, 0, ...]

    // A_y = 1/ dy^2 [ [ -2, 0,  0,  ...],
    //               [  0, -2,  0,  0, width zeros later, +1, ...]
    //               [  0, 0, -2, 0, 0, ... ]
    //               [  0,  0, 0, -2, 0, 0, ...]

    // A = A_x + A_y


    max_iter := 100

    for it := 0; it < max_iter; it += 1 {
        // Do a Gauss Seidel iteration
        error := gauss_seidel(field, source)

        if error < 1e-3 {
            break
        }
    }

}


gauss_seidel :: proc(field: ^Field2D, source: ^Field2D) -> f64 {
    error := 0.0

    for row : u64 = 0; row < field.height; row += 1 {
       for col : u64 = 0; col < field.width; col += 1 {
           idx : u64 = col + row * field.width

           x_k := field.data[idx]
           b := source.data[idx]

           p1 := 0.0
           if idx >= 1 {
               p1 += a(idx, idx - 1, field.width) * field.data[idx - 1] 
           }
           if idx >= field.width {
               p1 += a(idx, idx - field.width, field.width) * field.data[idx - field.width] 
           }

           p2 := 0.0
           if idx + 1 < field.width * field.height {
               p2 += a(idx, idx + 1, field.width) * field.data[idx + 1] 
           }
           if idx + field.width < field.width * field.height {
               p2 += a(idx, idx + field.width, field.width) * field.data[idx + field.width] 
           }


           x_k1 := (b * field.dg * field.dg - p1 - p2) / a(idx, idx, field.width)

           field.data[idx] = x_k1

           error += x_k1 - b
       }
   } 
   error /= cast(f64) (field.width * field.height)

   return error
}


test_source :: proc(width: u64, height: u64, dg: f64) -> ^Field2D {
    source := new(Field2D)
    data := make([]f64, width * height)
    source.width = width
    source.height = height
    source.dg = dg
    source.data = data

    for row := 0; row < cast(int)source.height; row += 1 {
        for col := 0; col < cast(int)source.width; col += 1 {
            if math.abs(row - cast(int) source.height / 2) < 5 && math.abs(col - cast(int) source.width / 2) < 5 {
                source.data[col + row * cast(int)source.width] = 1.0
            } else if math.abs(row - cast(int) source.height / 2) < 10 && math.abs(col - cast(int) source.width / 2) < 10 {
                source.data[col + row * cast(int)source.width] = -0.5
            } else {
                source.data[col + row * cast(int)source.width] = 0.0
            }
        }
    }


    return source
}

random_source :: proc(width: u64, height: u64, dg: f64) -> ^Field2D {
    source := new(Field2D)
    data := make([]f64, width * height)
    source.width = width
    source.height = height
    source.dg = dg
    source.data = data

    for row := 0; row < cast(int)source.height; row += 1 {
        for col := 0; col < cast(int)source.width; col += 1 {
            source.data[col + row * cast(int)source.width] = rand.float64() * 2.0 - 1.0
        }
    }


    return source
}
