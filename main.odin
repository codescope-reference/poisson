package main

import "core:fmt"
import "core:math"
import rand "core:math/rand"
import SDL "vendor:sdl2"

WHEIGHT :: 450 
WWIDTH :: 800 

visualize_field :: proc(field: ^Field2D, renderer: ^SDL.Renderer) {
    square_width : u64 = 1 
    square_height : u64 = 1 

    min_value := 0.0
    max_value := 0.0
    for value, i in field.data {
        if i == 0 {
            min_value = value
            max_value = value
        } else {
            if value < min_value {
                min_value = value
            }

            if value > max_value {
                max_value = value
            }
        }
    }


    for i : u64 = 0; i < WHEIGHT; i += 1 {
        for j : u64 = 0; j < WWIDTH; j += 1 {
            f_j := (field.width * j) / WWIDTH 
            f_i := (field.height * i) / WHEIGHT 

            // Scale field value to go from 0 to 1
            field_value := field.data[f_i * field.width + f_j]
            corr_val := (field_value - min_value) / (max_value - min_value)

            blue := cast(u8) (255.0 * corr_val)
            red :=  cast(u8) (255.0 * (1.0 - corr_val))
            

            _ = SDL.SetRenderDrawColor(renderer, red, 0x00, blue, 0xff)
            _ = SDL.RenderFillRect(renderer, &SDL.Rect { x = cast(i32) (j * square_width), y =cast(i32)  (i * square_height), w = cast(i32) square_width, h =cast(i32)  square_height}) 
        }
    }
}


main :: proc() {
    field_w :: 80 * 2
    field_h :: 45 * 2
    field := make_field(field_w, field_h, 0.1)
    defer delete_field(field)
    // source := test_source(100, 100, 0.1)
    source := random_source(field_w, field_h, 0.1)
    defer delete_field(source)

    init_result := SDL.Init(SDL.INIT_VIDEO)

    window : ^SDL.Window
    renderer : ^SDL.Renderer
    result := SDL.CreateWindowAndRenderer(WWIDTH, WHEIGHT, SDL.WINDOW_SHOWN, &window, &renderer) 

    is_running := true
    event : SDL.Event
    for is_running {

        // t1 := SDL.GetTicks()
        gauss_seidel(field, source)
        // t2 := SDL.GetTicks()
        // fmt.println("Time for Gauss-Seidel: ", t2 - t1, "ms")
        visualize_field(field, renderer)
        // visualize_field(source, renderer)



        SDL.RenderPresent(renderer)

        for SDL.PollEvent(&event) {
            #partial switch event.type {
                case SDL.EventType.QUIT: {
                    is_running = false
                    break
                }
            }
        }

        // SDL.Delay(5)
    }
}
