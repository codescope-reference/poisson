package main 

import "core:math"
import rand "core:math/rand"

Field2D :: struct {
    data : []f64
    width : u64
    height : u64
    dg: f64
}

make_field :: proc(width: u64, height: u64, dg: f64) -> ^Field2D {
    field : ^Field2D = new(Field2D)
    field.width = width
    field.height = height
    field.dg = dg

    data := make([]f64, width * height)
    // for iy : u64 = 0; iy < height; iy += 1 {
        // for ix : u64 = 0; ix < width; ix += 1 {
            // x := cast(f64) ix
            // y := cast(f64) iy
            // // field_value := rand.float64()
            // field_value := math.sin(x * dg + y * dg)
            // idx := ix + iy * width
            // data[idx] = field_value
        // }
    // }


    field.data = data

    return field
}

delete_field :: proc(field: ^Field2D) {
    delete(field.data)
    free(field)
}
